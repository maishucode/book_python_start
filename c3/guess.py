import random
import time
ans = random.randint(1, 100)
print('欢迎进入猜数字游戏，答案是1到100之前的数字')
max_guess = 8
count = 0

begin = time.time()

while count < max_guess:
	count += 1
	print(f'第{count}轮')
	guess = int(input('猜猜？'))
	if guess == ans:
		print('猜对了！人生赢家！')
		break
	elif guess > ans:
		print('太大了！')
	elif guess < ans:
		print('太小了')
else:
	print('超出猜测次数')

end = time.time()
print(f'用时：{end-begin}')