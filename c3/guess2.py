import random
import time
MAX_GUESS = 10

while(True):
	answer = random.randint(1, 100) #生成1到100之间的随机数
	print(answer)
	print('欢迎进入猜数字游戏，答案是1到100之前的数字')
	count = 0
	begin_time = time.time()
	while(True):
		if count == MAX_GUESS:
			print('超过猜测次数限制，游戏结束!')
			break
		else:
			count += 1

		guess_str = input(f'第{count}次猜测：')

		if guess_str.isdigit():
			guess = int(guess_str)
		else:
			print('输入的不是数字，跳过..')
			continue

		if answer == guess:
			print('太棒了，你赢了')
			break
		elif guess > answer:
			print('不对，太大了！')
		else:
			print('不对，太小了')

	end_time = time.time()
	used_time = end_time - begin_time
	print(f'用时：{round(used_time,2)}秒')	
	order = input('要继续玩吗？y表示继续，其他键退出：')
	if order != 'y':
		print('再见！')
		break


