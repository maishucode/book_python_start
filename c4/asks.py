from datetime import datetime
import random
import requests
import json


def show_time():
	dt = datetime.now()
	print(dt.strftime('今天是:%Y年%m月%d日 %H:%M:%S'))	

def show_luck():
	luck = random.randint(1, 10)
	if luck < 5:
		print('运势欠佳，建议休养生息')
	elif luck >=5 and luck <= 7:
		print('运势平稳，建议稳步前行')
	else:
		print('运势强劲，建议开疆拓土')

def show_tianqi():
	url = 'http://www.weather.com.cn/data/sk/101010100.html'
	response = requests.get(url)
	response.encoding = response.apparent_encoding
	tq_json = json.loads(response.text)
	city = tq_json["weatherinfo"]["city"]
	wendu = tq_json["weatherinfo"]["temp"]
	print(f'#{city}的温度是：{wendu}')

def unknown_ask():
	print('对不起，这个命令我不懂，你可以跟我说：tianqi, time, luck或者886')
