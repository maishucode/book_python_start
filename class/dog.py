import random 
from human import Human

class Dog:
    def __init__(self, name, age):
        self.name = name 
        self.age = age 
    
    def eat(self):
        print("{} is eating".format(self.name))

    def run(self):
        print("{} is running".format(self.name))
    
    def bark(self):
        print("{}：汪汪汪！".format(self.name))


class HuntDog(Dog):
    def __init__(self, name, age, power, owner):
        super().__init__(name, age)
        self.power = power
        self.owner  = owner
    
    def hunt(self):
        if self.power < 5:
            print('我太小了，还不能捕猎')
        else:
            luck = random.randint(1, 10)
            if luck > 5:
                print(f"抓到一只小🐇")
            else:
                print('运气不好，没抓到...')

    def run(self):
        print("{}是猎狗，跑的特别快！".format(self.name))

zhangsan = Human('张三', 170, 70)
hdog = HuntDog('大黑', 5, 8, zhangsan)
print(f'{hdog.name}的主人是：{hdog.owner.name}') 
