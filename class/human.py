class Human:
    def __init__(self, name, height, weight):
        self.name = name
        self.height = height
        self.weight = weight
        self._blood = 100

    def eat(self):
        print('{}在吃饭'.format(self.name))
        self.weight = self.weight + 0.01

    '''每次跑步x公里，会增加x的血量，但最多增加到120'''
    def run(self, distance):
        print('{}跑了{}公里，真棒👍'.format(self.name, distance))
        self._blood = self._blood + distance
        if self._blood > 120:
            self._blood = 120

    def blood(self):
        return self._blood

if __file__ == '__main__':
    maishu = Human('麦叔', 170, 70)
    print(maishu.blood())
    maishu.run(10)
    print(maishu.blood())
    print(maishu.blood())
    maishu.run(20)
    print(maishu.blood())




