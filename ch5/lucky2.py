import sys
import random
NUM = 6
scores = []

is_vip = False
if(len(sys.argv) > 1) and sys.argv[1] == 'VIP':
	is_vip = True
	print('=======欢迎进入幸运大转盘VIP游戏======')
else:
    print('=======欢迎进入幸运大转盘游戏======')


for n in range(NUM):
    input(f"第{n+1}次，敲回车键抽取幸运数字")
    if is_vip:
        luck_num = random.randint(6, 18)
    else:
        luck_num = random.randint(1, 12)
    scores.append(luck_num)
    print(luck_num)

print('您的得分如下：')
for index, s in enumerate(scores, start=1):
    print(f'第{index}局：{s}')    

total = sum(scores)
avg = total / NUM
round_avg = round(avg, 1)
print(f'您的平均分：{round_avg}')

print(max(scores))
print(min(scores))
