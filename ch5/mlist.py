print('========创建list的三种方法==========')
print('1. 字面量')
names = ['张三', '李四', '王五']
print(names)
empty_names = []
print(empty_names)
things = ['张三', '李四', 94, 83.5]
print(things)
print('2. 构造方法')
scores = list()

print('3. 用构造方法转换其他的序列')
r = range(1, 100)
print(r)
print(type(r))
nums = list(r)
print(type(nums))
print(nums)

print(list('HelloWorld'))
#print(list(5)) 会报错，因为只有iterable的对象可以被转化

print('========添加元素的方法==========')
print(names)
names.append('赵六')
print(names)
names.insert(0, '王老大')
print(names)
names.insert(2, '王老小')
print(names)
names.insert(-1, '赵老七')
print(names)

names2 = ['tom', 'mike', 'jerry']
names.append(names2)
print(names)
print(len(names))

print(names + names2)
#print(names + 8) list只能和list相加
print(names)
names.extend(names2)
print(names)

print('========删除元素的方法==========')
print(names)
del names[2]  # del是delete的缩写
print(names)
#del names[88] 删除一个不存在的下标会出现IndexError

#我们可以先判断来防止出错
if len(names) > 88:
    del names[88]

n = names.pop()
print(n)
print(names)

n = names.pop(3)
print(n)
print(names)

n = names.remove('赵六')
print(n)  # remove没有返回值，所以打印是None
print(names)

names3 = ['tom', 'mike', 'tom', 'jerry', 'tom']
names3.remove("tom")
print(names3)
#删掉所有的tom
names3 = ['tom', 'mike', 'tom', 'jerry', 'tom']
while 'tom' in names3:
    names3.remove('tom')
print(names3)

print('========修改房间住的变量的方法==========')
print(names)
names[1] = 'Mary'
print(names)

#给一个不存在的房间赋值，也是IndexError
#names[18] = 'Mary'
#把所有住tom的房间都换成Mary
names3 = ['tom', 'mike', 'tom', 'jerry', 'tom']
for index, n in enumerate(names3):
    if n == 'tom':
        names3[index] = 'Mary'
print(names3)

print('========查询list的方法==========')
print(len(names))
#数组的下标，从前到后是0开始，从后往前是-1开始
# [1, 2, 4, 6, 8, 2]
#  0, 1, 2, 3, 4, 5
# -6 -5 -4 -3 -2 -1
print(names)
print(names[0])
print(names[-1])
#下标越界，会出现IndexError
#print(names[9])

#通过index查询一个元素的下标
print(names.index('tom'))
#index查询下标，如果没有，会抛出ValueError
print(names.index('tommy'))
#可以通过in, not in查询一个元素在不在列表中
if 'tom' not in names:
    names.append('tom')
else:
    print(names.index('tom'))
#通过len查询列表的长度
print(len(names))
