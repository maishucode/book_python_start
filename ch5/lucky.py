import random
import sys
scores = []
times = 6

#判定是否是VIP模式，默认设置为False
is_vip = False
if(len(sys.argv) > 1) and sys.argv[1] == 'VIP':
	is_vip = True 
	print('=======欢迎进入幸运大转盘VIP游戏======')
else:
    print('=======欢迎进入幸运大转盘游戏======')


for n in range(times):
	input(f"第{n+1}次，敲回车键抽取幸运数字")
	#如果是VIP模式，使用比较大的范围
	if is_vip:
		s = random.randint(6, 18)
	else:
		s = random.randint(1, 12)

	scores.append(s)
	print(f'你的幸运数字是{s}')

#循环打印游戏分数，并计算总分
print('游戏结束，你的分数如下:')
for index, s in enumerate(scores):
	print(f'第{index}轮：{s}')

#计算平均分并打印
total = sum(scores)
print(f'平均分：{round(total/len(scores), 2)}')

print('=======GAME OVER=======')
